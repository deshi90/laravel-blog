<?php

namespace App;

use View;
use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
//use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\Redirect;

use Session;

class User extends Model {
	protected $fillable = ['username', 'password', 'email'];
	public $timestamps = false;

	public function loginUser ($username, $password) {

		$users = User::select ('username', 'password')->where ('username', 'LIKE', $username)->take (1)->
		where ('password', 'LIKE', $password)->
		get ();

		if (!$users->isEmpty ()) {
			
			foreach ($users as $user) {

				Session::set ('username', $user->username);
			}

			return Redirect::to ('article');
			

		} else {

			Session::flush ();
			return redirect ()->action ('UserController@login')->with ('message', 'invalid login');
		}

	}

	public function registerUser ($username, $password, $email) {
		if (empty($username) || empty($password) || empty($email)) {
			return redirect ()->action ('UserController@login');

		} else {
			User::create (['username' => $username, 'password' => $password, 'email' => $email]);
			$data = [
				'username' => $username,
				'email' => $email
			];
			return view ('success')->with ($data);

		}

	}
}