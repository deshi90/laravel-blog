<?php

use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get ('/login', 'UserController@login');

Route::get ('/register', 'UserController@register');
Route::get ('/postLogin', 'UserController@postLogin');
Route::get ('/postRegister', 'UserController@postRegister');

Route::post ('/checkLogin',

	array ('before' => 'csrf', function () {


		$rules = array (
			'username' => 'required|Min:3|Max:8|exists:users,username',
			'password' => 'required|Min:7|exists:users,password'
		);

		$validation = Validator::make (Input::all (), $rules);


		if ($validation->fails ()) {
			// Validation has failed.
			return Redirect::to ('login')->withInput ()->withErrors ($validation);
		} else {
//			Session::flush();
			Session::put('username',Input::get('username'));
			Session::put('password',Input::get('password'));

			return redirect ()->action ('UserController@postLogin');
		}

	}));
Route::post ('/checkRegister',
	array ('before' => 'csrf', function () {
		$rules = array (
			'username' => 'required|Min:3|Max:8|Unique:users,username',
			'password' => 'required|AlphaNum|Min:7|Confirmed',
			'password_confirmation' => 'required|AlphaNum|',
			'email' => 'Required|Between:3,64|Email|Unique:users');

		$validation = Validator::make (Input::all (), $rules);

		if ($validation->fails ()) {
			// Validation has failed.
			return Redirect::to ('register')->withInput ()->withErrors ($validation);
		} else {

			Session::put('username',Input::get('username'));
			Session::put('password',Input::get('password'));
			Session::put('email',Input::get('email'));

			return redirect ()->action ('UserController@postRegister');
		}

		// Validation has succeeded. Create new user.
	}));

Route::get ('/article', 'ArticleController@allArticles');



