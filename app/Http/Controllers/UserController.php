<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;


class UserController extends Controller {
	protected $model;

	public function __construct (User $user) {
		$this->model = $user;
	}

	public function login () {


		return view ('forms.login');
	}

	public function register () {
		return view ('forms.register');
	}

	public function postLogin () {


		$username = Session::get ('username');
		$password = Session::get ('password');
		Session::flush ();


		return $this->model->loginUser ($username, $password);

	}

	public function postRegister () {
		$username = Session::get ('username');
		$password = Session::get ('password');
		$email = Session::get ('email');
		Session::flush ();

		return $this->model->registerUser ($username, $password, $email);

	}
}
