<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Session;

class ArticleController extends Controller {
	public function __construct (Article $model) {
		$this->model = $model;
	}


	public function allArticles () {

		if (Session::get ('username') == null) {
			return Redirect::to ('login');
		} else {
			return $this->model->allArticle();
		}

	}

}
