<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Article extends Model {

	public function allArticle () {

		echo Session::get('username');

		$articles = Article::leftjoin('users','articles.user_id', '=', 'users.id')->
								select('articles.id','articles.title','articles.content','articles.date','users.username')->get();


		return view ('article.listArticles')->with ('articles', $articles);
	}


}
