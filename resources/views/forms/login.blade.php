@extends('app')

@section('title')
    Login Form
@stop

@section('login')

    <div class="login">
        <h1>Login to System</h1>

        {!! Form::open(['url'=>'checkLogin','method'=>'post']) !!}

        <p>{!! Form::text('username',null,['placeholder'=>'Username','required'=>'required'], Input::old('username'))
            !!}
            {{ $errors->first('username') }}
            @if (session('message'))

                {{ session('message') }}

            @endif

        </p>

        <p>{!! Form::password('password',['placeholder'=>'Password','required'=>'required']) !!}</p>

        <p class="submit">
            {!! Form::submit('Login',['name'=>'commit']) !!}
        </p>

        <p><a href="register">Go to Register!</a></p>
        {!! Form::close() !!}



        {{--<form method="post" action="/postLogin">--}}

        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        {{--<p><input type="text" name="username" value="" placeholder="Username"></p>--}}

        {{--<p><input type="password" name="password" value="" placeholder="Password"></p>--}}

        {{--<p class="submit">--}}

        {{--<input type="submit" name="commit" value="Login"></p>--}}

        {{--<p><a href="/register">Go to Register!</a></p>--}}
        {{--</form>--}}
    </div>


@stop