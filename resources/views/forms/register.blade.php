@extends('app')

@section('title')
    Register Form
@stop


@section('register')

    <div class="login">
        <h1>Register to System</h1>


        {!! Form::open(['url'=>'checkRegister' ,'method'=>'post']) !!}

        <p> {!! Form::text('username',null,['placeholder'=>'Username','required'=>'required'],Input::old('username')) !!}
            {{ $errors->first('username') }}  </p>

        <p> {!! Form::password('password',['placeholder'=>'Password','required'=>'required'],Input::old('password')) !!}
            {{ $errors->first('password') }}</p>

        <p> {!! Form::password('password_confirmation',['placeholder'=>'Re-Password','required'=>'required']) !!}
            {{ $errors->first('password_confirmation') }}</p>

        <p> {!! Form::text('email',null,['placeholder'=>'E-mail','required'=>'required'],Input::old('email')) !!}
            {{ $errors->first('email') }}</p>

        <p class="submit">  {!! Form::submit('Login',['name'=>'commit']) !!}</p>

        <p><a href="/login">Go to Login!</a></p>
        {!! Form::close() !!}

        {{--<form method="post" action="/postRegister">--}}

        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        {{--<p><input type="text" name="username" value="" placeholder="Username"></p>--}}

        {{--<p><input type="password" name="password" value="" placeholder="Password"></p>--}}

        {{--<p><input type="password" name="repassword" value="" placeholder="Re-Password"></p>--}}

        {{--<p><input type="text" name="email" value="" placeholder="E-mail"></p>--}}


        {{--<p class="submit"><input type="submit" name="commit" value="Register!"></p>--}}

        {{--<p><a href="/login">Go to Login!</a></p>--}}
        {{--</form>--}}
    </div>



@stop