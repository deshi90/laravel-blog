@extends('app')


@section('success')
<p style="color: red">     You are successfully registered as {{ $username }} with email :{{ $email }}.Please <a href="/login">login</a>   with your username and password!</p>
@stop
