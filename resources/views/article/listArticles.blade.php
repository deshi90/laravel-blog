@extends( 'article' )

@section('title' )
    All Articles
@stop

@section('content' )

    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>content</th>
            <th>Author</th>
            <th>date</th>
            <th colspan="2">action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)

            <tr class="article-{{ $article->id }}">
                <td>   {{ $article->id }}  </td>
                <td>   {{ $article->title }}  </td>
                <td>   {{ $article->content }}  </td>
                <td>  {{ $article->username }}  </td>
                <td>   {{ $article->date }} </td>
                <td> <a  href="index.php?page=article&action=deleteArticle&id='{{$article->id }} '" class="delete" >Delete</a></td>
                <td> <a  id="jquery" href="index.php?page=article&action=getEditArticle&id='{{$article->id }}'" >Edit</a></td>
            </tr>

        @endforeach
        </tbody>
    </table>

@stop
